//
//  Movie.swift
//  OMDB Demo
//
//  Created by Marcelino on 18/11/15.
//  Copyright © 2015 IPLeiria. All rights reserved.
//

import Foundation

struct Movie {
    
    let title:String
    let year:String
    let type:String
    let poster:String?

}