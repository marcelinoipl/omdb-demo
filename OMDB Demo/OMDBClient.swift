//
//  OMDBClient.swift
//  OMDB Demo
//
//  Created by Marcelino on 18/11/15.
//  Copyright © 2015 IPLeiria. All rights reserved.
//

import Foundation

class OMDBClient {
    
    static func searchMovieBy(query:String?, completionHandler:([Movie]?, NSError?) -> Void) -> Void {
        guard var queryString = query else {
            return
        }
        queryString = queryString.stringByReplacingOccurrencesOfString(" ", withString: "+")
        guard let url = NSURL(string: "http://www.omdbapi.com/?s=" + queryString) else {
            return
        }
        let session = NSURLSession.sharedSession()
        let sessionTask = session.dataTaskWithURL(url) { (data, response, error) -> Void in
            // parse data
            let movies = parseMovies (data)
            completionHandler (movies, error)
        }
        
        // Connect url
        sessionTask.resume()
        
    }
    
    static func parseMovies(data:NSData?) ->[Movie]? {
        guard let dta = data else {
            return nil
        }
        do {
            if let searchDic = try NSJSONSerialization.JSONObjectWithData(dta, options: NSJSONReadingOptions.AllowFragments) as? NSDictionary {
                
                var movies = [Movie]()
                
                if let moviesJsonArray = searchDic.objectForKey("Search") {
                    for (var i = 0 ; i < moviesJsonArray.count ; i++) {
                        if let movieJson = moviesJsonArray.objectAtIndex(i) as? NSDictionary {
                            let title = movieJson.objectForKey("Title") as! String
                            let poster = movieJson.objectForKey("Poster") as! String
                            let type = movieJson.objectForKey("Type") as! String
                            let year = movieJson.objectForKey("Year") as! String
                            movies.append(Movie(title:title, year: year, type: type, poster: poster))
                        }
                    }
                }
                return movies
            }
        }
        catch {
            
        }
        return nil
    }
}